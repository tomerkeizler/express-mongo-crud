import mongoose from "mongoose";
const { Schema, model } = mongoose;

const UserSchema = new Schema({
  first_name: {
    type: String,
    validate: {
      validator: (v) => /^[A-Za-z]+$/.test(v),
      message: "First name should only contain alphabetical characters",
    },
    minlength: 2,
    maxlength: 20,
    required: true,
  },
  last_name: {
    type: String,
    validate: {
      validator: (v) => /^[A-Za-z]+$/.test(v),
      message: "Last name should only contain alphabetical characters",
    },
    minlength: 2,
    maxlength: 20,
    required: true,
  },
  email: { type: String, required: true, unique: true },
  phone: {
    type: String,
    validate: {
      validator: (v) => /^\d{3}-\d{3}-\d{4}$/.test(v),
      message: (props) => `${props.value} is not a valid phone number`,
    },
    required: true,
  }
});

export default model("user", UserSchema);
