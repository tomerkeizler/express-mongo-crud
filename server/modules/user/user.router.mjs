/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.mjs";
import user_model from "./user.model.mjs";
import express from "express";
import log from "@ajar/marker";
import { optional_user_schema, required_user_schema } from "./user.schema.mjs";
import validation_handler from "../../middleware/validation.handler.mjs";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATE A NEW USER
// router.post("/", async (req, res,next) => {
//    try{
//      const user = await user_model.create(req.body);
//      res.status(200).json(user);
//    }catch(err){
//       next(err)
//    }
// });

// CREATES A NEW USER
router.post(
  "/",
  validation_handler(required_user_schema),
  raw(async (req, res) => {
    log.obj(req.body, "create a user, req.body:");
    const user = await user_model.create(req.body);
    res.status(200).json(user);
  })
);

// CREATES MANY USERS
router.post(
  "/many",
  raw(async (req, res) => {
    log.obj(req.body, "create many users, req.body:");
    const users = await user_model.insertMany(req.body);
    res.status(200).json(users);
  })
);

// GETS ALL USERS
router.get(
  "/",
  raw(async (req, res) => {
    const users = await user_model.find();
    // .select(`-_id
    //         first_name
    //         last_name
    //         email
    //         phone`);
    res.status(200).json(users);
  })
);

router.get(
  "/paginate/:page?/:items?",
  raw(async (req, res) => {
    log.obj(req.params, "get all users, req.params:");
    let { page = 0, items = 10 } = req.params;
    const users = await user_model
      .find()
      .select(`first_name last_name email phone`)
      .limit(parseInt(items))
      .skip(parseInt(page * items));

    res.status(200).json(users);
  })
);

// GETS A SINGLE USER
router.get(
  "/:id",
  raw(async (req, res) => {
    const user = await user_model.findById(req.params.id);
    // .select(`-_id
    //     first_name
    //     last_name
    //     email
    //     phone`);
    if (!user) return res.status(404).json({ status: "No user found." });
    res.status(200).json(user);
  })
);

// UPDATES A FULL SINGLE USER
router.put(
  "/:id",
  validation_handler(required_user_schema),
  raw(async (req, res) => {
    const user = await user_model.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      upsert: false,
    });
    res.status(200).json(user);
  })
);

// UPDATES ONLY SOME FIELDS OF A SINGLE USER
router.patch(
  "/:id",
  validation_handler(optional_user_schema),
  raw(async (req, res) => {
    const user = await user_model.findById(req.params.id);
    if (!user) return res.status(404).json({ status: "No user found." });

    for (const [key, value] of Object.entries(req.body)) {
      if(value)
      user[key]=value
    }

    log.green(user)

    const newUser = await user_model.findByIdAndUpdate(req.params.id, user, {
      new: true,
      upsert: false,
    });
    res.status(200).json(newUser);
  })
);

// DELETES A USER
router.delete(
  "/:id",
  raw(async (req, res) => {
    const user = await user_model.findByIdAndRemove(req.params.id);
    res.status(200).json(user);
  })
);

export default router;
