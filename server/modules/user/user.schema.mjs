import joi from "joi";

const name = joi
  .string()
  .pattern(/^[A-Za-z]+$/)
  .min(2)
  .max(20);

const email = joi
  .string()
  .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } });

const phone = joi.string().pattern(/^\d{3}-\d{3}-\d{4}$/);

// ------------------------------------

const optional_user_schema = joi.object({
  first_name: name.optional().allow(""),
  last_name: name.optional().allow(""),
  email: email.optional().allow(""),
  phone: phone.optional().allow(""),
});

// ------------------------------------

const required_user_schema = joi.object({
  first_name: name.required(),
  last_name: name.required(),
  email: email.required(),
  phone: phone.required(),
});

export { optional_user_schema, required_user_schema };
